function Fc1 = lowcutof(FM,X, rect)
%%
%%%%%%%%%%%%%%%%%   Find lower cut-off frequency    %%%%%%%%%%%%%%%%%%%%%%%
% This script determines the low cut-off frequency for filtering procedure 
% in peak period estimation of near-shore waves using pixel intensity time
% series by determinig the main frequency of noisy signals associated to
% environmental brightness fluctuations.
%     INPUT:
%             FM : Sampling frequency of timestack image
%             X : Pixel intensity time series of the whole timestack image
%      OUTPUT:
%             Fc1 : Low cut-off frequency (main frequency of noisy signal)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%  Selecting land area %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
serieX = imcrop(X,rect);
serieX = double(serieX);
%%%%% Display land area average pixel intensity time series
% figure
% plot([0:1:length(mean(serieX,1))-1]*1/240,mean(serieX,1));
%%
%%%%%%%%%%%%%%%%%%  Bandpass filtering %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h = filter_bandpass3(FM); % Filtering frequencies between 0.03 and 0.5 Hz
SerieC = filtfilt(h.sosMatrix,h.ScaleValues,serieX');
SerieC = SerieC';
serieX3=SerieC;
%%
%%%%%%%%%%%%%%%%%   Windowing   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
w = blackman(size(serieX3,2)); % Creating window 
windows = [];
for j = 1:size(serieX3,1)
    windows(j,:) = w';
end
serieX2 = serieX3.*windows(1:size(serieX3,1),1:size(serieX3,2)); % Windowing signal
%%
%%%%%%%%%%%%%%%%%   Spectral analysis %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = size(SerieC,2); 
Np=2^nextpow2(N);
SP = [];
SP2 = [];
fref=2*((FM/2)/N)*(0:N/2-1);
for i=1:size(serieX2,1)     
    try
        fo= fft(serieX2(i,:),N);
    catch 
        disp(i);
    end
    F1= fo(1:floor(end/2)).*conj(fo(1:floor(end/2)));
    F2=F1/(N/2);
    F2(1)=F1(1)/N;
    F3 = F2/(2*((FM/2)));
    SP(:,i) = F3;
    if isnan(F3)
        disp('es nan')
    end
end
mediaSP=mean(SP,2);
% figure % Plot land pixel spectrum
% plot(fref,mediaSP)
% find peak
[val ind2] = max(mediaSP); % Finding peak of spectrum
Fc1 = fref(ind2)*1.3;   % Aggrandizing by 30% to account for peak width
end
