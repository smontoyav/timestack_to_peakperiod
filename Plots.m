%   This Script generates Pixel intensity time series processing images of 
%   Unfiltered time series - (a) Pixel intensity series and (b) Frequency spectrum; 
%   Filtered time series - (c) Pixel intensity series and (d) Frequency spectrum; 
%   and Windowed time series - (e) Pixel intensity series and (f) Frequency spectrum.
close all
clear all
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% INPUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
path = ''; 
% Path of the timestack image to be analized
FM = 4; % Sampling frequency of the timestack image (Hz)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%% PROCESSING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

imagen = imread(path); %Read the image
serieX11 = imrotate(imagen,-90); %Rotates 90 degrees, coment if timestack displays horizontally
serieX = rgb2gray(serieX11); %Transform to grayscale
Pixint = serieX; %pixel intesity series
Pixint_mean = mean(serieX,1); %Average pixel intesity series for plot (a) 

Fc1 = lowcutof(FM,serieX); %Find low cut-off frequency from land pixels (Set adequate ROI in lowcutof.m)

%%
%%%%%%%%% TRESHOLDING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
I = uint8(serieX); %This line defines matrix of pixel intesity series
[a em]=graythresh(I);   %Modify by AOsorio to find the effectiveness of thresholding
J=im2bw(I,a);
serie3 = double(J*255);
[fil col] = size(serieX);
serie4=uint8(serie3);
posinicial=[];
posfinal = [];
for i=1:fil
    indposinit = find(serie3(i,:) == 0);  
    if(length(indposinit) >= col * 0.70) 
        if isempty(posinicial)
            posinicial = i;
        end
    else   
       if isempty(posfinal) & i > posinicial
           indposfinal = find(serie3(i,:) == 255);
           if(length(indposfinal) >= col * 0.02)
               posfinal = i;
           end
       end
    end
end
if isempty(posfinal)
    for i=1:fil
       if isempty(posfinal) & i > posinicial
           indposfinal = find(serie3(i,:) == 255);
           if(length(indposfinal) >= col * 0.02)
               posfinal = i;
           end
       end
    end
end
serieXX2=double(serieX(posinicial:posfinal,:)); % Remaining data after tresholding
serieX = serieXX2;
Pixint_TH = serieX; % Intesity series after tresholding
Pixint_THmean = mean(serieX,1); %Mean intesity series after tresholding
%%
%%%%%%%%%%%% Filtering %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h = filter_bandpass2(FM, Fc1); %Creates filter
SerieC = filtfilt(h.sosMatrix,h.ScaleValues,serieX'); 
SerieC = SerieC';
Pixint_filt = SerieC;
Pixint_filtmean = mean(SerieC,1); %Mean pixel intesity serie after filtering
%%
%%%%%%%%%%% Windowing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
col=size(serieX,2);
serieX3=SerieC;
w = blackman(size(serieX3,2)); 
windows = [];
for j = 1:size(serieX3,1)
    windows(j,:) = w';
end
serieX2 = serieX3.*windows(1:size(serieX3,1),1:size(serieX3,2));
Pixint_win = serieX2; % Windowed pixel intesity series
Pixint_winmean = mean(serieX2,1); % Windowed mean pixel intesity series
%%
%%%%%%%%%%%% Frequencies %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = size(SerieC,2); 
Np=2^nextpow2(N);
SP = [];
fref=2*((FM/2)/N)*(0:N/2-1); %Frequencies for FFT analysis
%%
%%%%%%%%%%% FFT after treshold %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:size(Pixint_TH,1)
    try
        fo= fft(Pixint_TH(i,:),N);
    catch 
        disp(i);
    end
    F1= fo(1:floor(end/2)).*conj(fo(1:floor(end/2)));
    F2=F1/(N/2);
    F2(1)=F1(1)/N;
    F3 = F2/(2*((FM/2)));
    SP(:,i) = F3;
    if isnan(F3)
        disp('es nan')
    end
end
mediaSP=mean(SP,2);
FFT_raw = mediaSP;
SP = [];
%%
%%%%%%%%%%% FFT filtered %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:size(Pixint_filt,1) 
    try
        fo= fft(Pixint_filt(i,:),N);
    catch 
        disp(i);
    end
    F1= fo(1:floor(end/2)).*conj(fo(1:floor(end/2)));
    F2=F1/(N/2);
    F2(1)=F1(1)/N;
    F3 = F2/(2*((FM/2)));
    SP(:,i) = F3;
    if isnan(F3)
        disp('es nan')
    end
end
mediaSP=mean(SP,2);
FFT_filt = mediaSP;
SP = [];
%%
%%%%%%%%%%% FFT Windowed %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:size(Pixint_win,1) 
    try
        fo= fft(Pixint_win(i,:),N);
    catch 
        disp(i);
    end
    F1= fo(1:floor(end/2)).*conj(fo(1:floor(end/2)));
    F2=F1/(N/2);
    F2(1)=F1(1)/N;
    F3 = F2/(2*((FM/2)));
    SP(:,i) = F3;
    if isnan(F3)
        disp('es nan')
    end
end
mediaSP=mean(SP,2);
FFT_win = mediaSP;
%%
%%%%%%%%%%%%%%%%%% PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%% Unfiltered time series (a) Pixel intensity series %%%%%%%%%%%%%%
figure(1);
plot(Pixint_THmean,'k-');
xlabel('Frames', 'FontName', 'Times New Roman', 'FontSize', 11) % x-axis label
ylabel('Pixel Intesity', 'FontName', 'Times New Roman', 'FontSize', 11) % y-axis label
set(gca, 'FontName', 'Times New Roman', 'FontSize', 11)

%%%%%%%%% Unfiltered time series (b) Frequency spectrum %%%%%%%%%%%%%%%%%%
figure(2);
plot(fref, FFT_raw,'k-');
xlim([0 0.05])
xlabel('Frequency (Hz)', 'FontName', 'Times New Roman', 'FontSize', 11) % x-axis label
ylabel('Spectral Density', 'FontName', 'Times New Roman', 'FontSize', 11) % y-axis label
set(gca, 'FontName', 'Times New Roman', 'FontSize', 11)

%%%%%%%%% Filtered time series - (c) Pixel intensity series %%%%%%%%%%%%%%
figure(3);
plot(Pixint_filtmean,'k-');
xlabel('Frames', 'FontName', 'Times New Roman', 'FontSize', 11) % x-axis label
ylabel('Pixel Intesity', 'FontName', 'Times New Roman', 'FontSize', 11) % y-axis label
set(gca, 'FontName', 'Times New Roman', 'FontSize', 11)

%%%%%%%%% Filtered time series -  (d) Frequency spectrum%%%%%%%%%%%%%%%%%%%
figure(4);
plot(fref, FFT_filt,'k-');
xlim([0 1])
xlabel('Frequency (Hz)', 'FontName', 'Times New Roman', 'FontSize', 11) % x-axis label
ylabel('Spectral Density', 'FontName', 'Times New Roman', 'FontSize', 11) % y-axis label
set(gca, 'FontName', 'Times New Roman', 'FontSize', 11)

%%%%%%%%% Windowed time series - (e) Pixel intensity series %%%%%%%%%%%%%%
figure(5);
plot(Pixint_winmean,'k-');
xlabel('Frames', 'FontName', 'Times New Roman', 'FontSize', 11) % x-axis label
ylabel('Pixel Intesity', 'FontName', 'Times New Roman', 'FontSize', 11) % y-axis label
set(gca, 'FontName', 'Times New Roman', 'FontSize', 11)

%%%%%%%%% Windowed time series -  Frequency spectrum %%%%%%%%%%%%%%%%%%%%%
figure(6);
plot(fref, FFT_win,'k-');
xlim([0 1])
xlabel('Frequency (Hz)', 'FontName', 'Times New Roman', 'FontSize', 11) % x-axis label
ylabel('Spectral Density', 'FontName', 'Times New Roman', 'FontSize', 11) % y-axis label
set(gca, 'FontName', 'Times New Roman', 'FontSize', 11)

