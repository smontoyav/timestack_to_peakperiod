function Tp = peak_period( path, name, type, rect, varargin)

%%%%%%%%%%%%%%%%%%%% DESCRIPTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script estimates the peak period of near-shore waves based on image
% analysis techniques and spectral analysis (FFT) of pixel intensity time
% series obtained from video records or timestack image of near-shore
% environments.
% Input:
%      'path': Path where the timestack or video are located.
%      'name': name of the file containing the timestack or video.
%      HORUS system uses the following format
%      YY.MM.DD.min.sec.msec.GMT.STATION.CAMERA.STACK.U.V.WXH.SYSTEM.avi
%      Where:
%            YY = year 
%            MM = month (01 to 12)
%            DD = day (01 to 31)
%            min = minutes (00 to 59)
%            sec = seconds (00 to 59)
%            msec = mili-seconds (00 to 99)
%            GMT = indicates GMT system for time
%            STATION = name of the station of the captured video
%            CAMERA = identifies the camera used for the capture
%            STACK = defines the type of file to be captured by the
%                    monitoring system, in this case STACK refers to video
%            U = Horizontal position of the ROI [pixels]
%            V = Vertical position of the ROI [pixels]
%            WXH = width (W) by height (H) of the ROI
%            SYSTEM = Name of the monitoring system that records the files
%      'type': 'video' or 'image'.
%      'rect': vector [xmin ymin width height]
%      Where:
%            xmin : Initial position for land ROI in the horizontal direction
%            ymin : Initial position for land ROI in the vertical direction
%            width : Width of land ROI in pixels, usually the same size of X
%            height : height of land ROI in pixels
%            rect = [1 0 4499 500] for Sylt
%            rect = [1 350 4499 100] for Bealearic island
%            rect = [1 600 1900 150] for Cartagena
%      'varargin': Sampling frequency (frame rate)of the timestack 
%                  must be given only for type = 'image'.
%
% Output:
%       'Tp': Peak period estimation of near-shore waves
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Original script written by Cesar Augusto Cartagena Ocampo for the M.Sc. 
% thesis entitled "Estimación del espectro direccional del oleaje mediante 
% procesamiento digital de imágenes" presented at the National University
% of Colombia.
% Copyright 2014 
% $Date: 2014/21/09 18:00 $
% Modified in 2018 by S. Montoya-Vargas to include low cut-off filtering
% procedures and general window function.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

Tp = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Reading timestack %%%%%%%%%%%%%%%%%%%%%%%%%%%%
if  strcmpi(type,'video') == 1
    video=mmreader(fullfile(path,name)); % Read video .avi 
    nFrames=video.NumberofFrames; % Determines number of frames
    vidHeight = video.Height; % Frame height [Pixel]
    vidWidth = video.Width; % Frame width [Pixel]
    FM = video.frameRate; % Sampling frequency or frame rate of the video 

    movgris(1:nFrames) = struct('cdata', zeros(vidHeight, vidWidth, 1,...
        'uint8'),'colormap', []); % Creates structure for frame storage
    serieX=[]; % Pixel intensity time series for each frame
    
    %%%% Determines the date of the record based on file name structure
    parts = regexp(name, '\.', 'split'); 
    year2 = 2000 + str2double(parts{1});
    if year2 > year(now)
        year2 = year2 - 100;
    end
    %%%% Convert video into timastack image (horizontal axis for time/frames)
        for i=1:nFrames
            movgris(i).cdata=rgb2gray(read(video,i));
            cont = 1;
            for j=5:size(movgris(i).cdata,1)-5
                ind = find(movgris(i).cdata(j,:));
                serieX(cont,i) = mean(movgris(i).cdata(j,ind)); % Averages 
                % pixel intesity through the width of the video
                cont = cont + 1;
            end
        end
elseif strcmpi(type,'image') == 1
        imagen = imread(fullfile(path,name));
        FM = varargin{1};
        serieX11 = imrotate(imagen,-90); % Rotates timestack. Comment this 
        % line if the timestack is displayed horizontally by defect.
        serieX = rgb2gray(serieX11); % Convert to grayscale pixel intesity
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%% Determine Low cut-off frequency %%%%%%%%%%%%%%%%%%%%
%%%%%%% NOTE: modify lowcutoff.m for adequate land ROI 
Fc1 = lowcutof(FM,serieX, rect); % low cut-off frequency (Hz)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%% Oshu tresholding procedure %%%%%%%%%%%%%%%%%%%%%%%%%
I = uint8(serieX);
[a em]=graythresh(I); % Determines the threshold 

J=im2bw(I,a); % Binarization based on treshold value
serie3 = double(J*255); % Extract pixel intensity for the tresholded region
[fil col] = size(serieX);
serie4=uint8(serie3);

posinicial=[];
posfinal = [];
%%%%%%%%%% Tresholded region
for i=1:fil
    indposinit = find(serie3(i,:) == 0);  
    if(length(indposinit) >= col * 0.70) 
        if isempty(posinicial)
            posinicial = i;
        end
    else   
       if isempty(posfinal) & i > posinicial
           indposfinal = find(serie3(i,:) == 255);
           if(length(indposfinal) >= col * 0.02)
               posfinal = i;
           end
       end
    end
end
if isempty(posfinal)
    for i=1:fil
       if isempty(posfinal) & i > posinicial
           indposfinal = find(serie3(i,:) == 255);
           if(length(indposfinal) >= col * 0.02)
               posfinal = i;
           end
       end
    end
end

serieXX2=double(serieX(posinicial:posfinal,:)); 
serieX = serieXX2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%%%% Filtering procedure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h= filter_bandpass2(FM, Fc1); % Creates filter
SerieC = filtfilt(h.sosMatrix,h.ScaleValues,serieX'); % Filtering the signal
SerieC = SerieC'; % rearrange data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%%%% Windowing procedure %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
col=size(serieX,2);
serieX3=SerieC;
serieX2 = serieX3;
w = blackman(size(serieX3,2)); % Blackman window
windows = [];
for j = 1:size(serieX3,1)
    windows(j,:) = w';
end
serieX2 = serieX3.*windows(1:size(serieX3,1),1:size(serieX3,2)); % Windowing
N = size(SerieC,2); 
Np=2^nextpow2(N);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%%%% Spectral analysis (FFT) %%%%%%%%%%%%%%%%%%%%%%%%%%
SP = [];
fref=2*((FM/2)/N)*(0:N/2-1); % reference frequencies

for i=1:size(serieX2,1) 
    try
        fo= fft(serieX2(i,:),N);
    catch 
        disp(i);
    end
    F1= fo(1:floor(end/2)).*conj(fo(1:floor(end/2)));
    F2=F1/(N/2);
    F2(1)=F1(1)/N;
    F3 = F2/(2*((FM/2)));
    SP(:,i) = F3; % Set spectrum for each row
    if isnan(F3)
        disp('es nan') % Non data result
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% Peak period estimation %%%%%%%%%%%%%%%%%%%%%%%%
mediaSP=mean(SP,2); % Average spectrum among rows

[val ind2] = max(mediaSP); % Find peak in average spectrum

fp = fref(ind2); % Peak frequencty
Tp = 1/fp; % Peak period
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end